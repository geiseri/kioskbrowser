Kiosk Browser Specification
===========================

Minimal kiosk browser for single page applications or limited browsing.

Features
--------

* All configuration is static and provided via a JSON file passed in on the commandline
* Stateless, session cookies, only memory based cache
* Browser will not load without a valid configuration
* Permissions per url can operate in white/black list mode
* URL matching can be done via regular expressions
* Selective override of SSL errors based on host and error
* Permissions on navigation, cut/copy/paste and printing
* Permissons on browser features such as webcam, audio, geolocation, etc.
* Fullscreen mode with option to not allow exiting
* Enable/Disable UI elements based on permissions
* Enable/Disable exiting browser
* Download manager subject to permissions with sandboxed storage locations
* Downloads can be limited by mimetype
* Mimetype file handler plugins. Example RDP files.
* Permissions on file uploads
* Limited PDF support with PDF.js
* Proxy support 
* Start URL support
* Virtual keyboard support for touch only
* Touch support for scrolling and navigation
* Spell check (Limited to Qt 5.8 or greater)
* Adobe Flash (requires pepper plugin)

Permissions Engine Specification
================================

URL Matching Rules
------------------

* Full URL - match regexp of the entire string representation of the URL
* Hostname and port - Regexp based on QUrl::authority
* Hostname only - Regexp based on QUrl::host
* Port only - Regexp based on QUrl::port
* Scheme only - Regexp based on QUrl::scheme

Rule Proposal
-------------

```
{
    "Match" : "<regexp>",
    "Access" : {
        "<feature1>" : true,
        "<feature2>" : false
    }
}
```

Outstanding Issues and Questions
================================

* How to map json string values into actual enums for ``KioskBrowser::hasPermission(...)``
* How to use webengine scripts to download a PDF and feed it to PDF.js
* How to author the input JSON file
* How to organize the rules in a data structure


TODO list
=========

* KioskUtilities::searchEngineQuery - Make search engine configurable
* KioskBrowser::configureBrowserSettings - Add spell checker support


License
=======

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see https://www.gnu.org/licenses/.
