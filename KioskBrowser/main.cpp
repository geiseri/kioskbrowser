/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "kioskbrowser.h"
#include "kioskbrowsermainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setOrganizationName(QLatin1String("TCOS"));
    app.setApplicationName(QLatin1String("KioskBrowser"));

    int result = 1;
    if (KioskBrowser::instance()->configureBrowser(app.arguments().value(1, QLatin1String(":/data/defaults.json")))) {
        KioskBrowser::instance()->addWindow(new KioskBrowserMainWindow);
        result = app.exec();
    }
    KioskBrowser::instance()->cleanup();
    return result;
}
