QT += core gui widgets webenginewidgets

TARGET = KioskBrowser
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += KIOSK_CORE_DLL

SOURCES += main.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$top_builddir/KioskBrowserCore/release/ -lKioskBrowser
else:win32:CONFIG(debug, debug|release): LIBS += -L$$top_builddir/KioskBrowserCore/debug/ -lKioskBrowser
else:unix:!macx: LIBS += -L$$top_builddir/KioskBrowserCore/ -lKioskBrowser

INCLUDEPATH += $$top_srcdir/KioskBrowserCore
DEPENDPATH += $$top_srcdir/KioskBrowserCore

INSTALLS += target
target.path = /usr/bin
