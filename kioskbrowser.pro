TEMPLATE = subdirs
CONFIG += ordered

SUBDIRS += \
    KioskBrowserCore \
    KioskBrowser \
    BrowserTests

OTHER_FILES += \
    README.md \
    COPYING
