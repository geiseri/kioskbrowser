/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "kioskenummapper.h"
#include "kioskpermissions.h"
#include "kioskutilities.h"
#include "kioskwildcardrule.h"
#include <QCoreApplication>
#include <QString>
#include <QtTest>

class BrowserTestsTest : public QObject
{
    Q_OBJECT

public:
    BrowserTestsTest();

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void testUrlFixup();
    void testUrlMatching();
    void testEnumMapper();
    void testPermissions();
    void testMimeMatchPermissions();
};

BrowserTestsTest::BrowserTestsTest()
{
}

void BrowserTestsTest::initTestCase()
{
}

void BrowserTestsTest::cleanupTestCase()
{
}

void BrowserTestsTest::testUrlFixup()
{
    QCOMPARE(QUrl("http://news.google.com"), KioskUtilities::fixupUrlInput("news.google.com"));
    QCOMPARE(QUrl("http://www.google.com/search?q=garbage string"), KioskUtilities::fixupUrlInput("garbage string"));
    QCOMPARE(QUrl("https://test.com"), KioskUtilities::fixupUrlInput("https://test.com"));
}

void BrowserTestsTest::testUrlMatching()
{
    KioskWildcardRule rule1("http://www.test.com");
    KioskWildcardRule rule2("http://*.test.com");

    QVERIFY(rule1.isMatch(QUrl("http://www.test.com")));
    QVERIFY(!rule1.isMatch(QUrl("http://www.test.com/foobar?p=10")));
    QVERIFY(!rule1.isMatch(QUrl("http://foo.test.com")));

    QVERIFY(rule2.isMatch(QUrl("http://www.test.com")));
    QVERIFY(!rule2.isMatch(QUrl("http://www.test.com/foobar?p=10")));
    QVERIFY(rule2.isMatch(QUrl("http://foo.test.com")));
}

void BrowserTestsTest::testEnumMapper()
{
    KioskEnumMapper mapper;
    bool ok = false;
    QCOMPARE(mapper.stringToEnumValue(QLatin1String("WebAttribute::XSSAuditingEnabled"), &ok), 7);
    QVERIFY(ok);

    QCOMPARE(mapper.stringToEnumValue(QLatin1String("WebAttribute::BogusEnum"), &ok), -1);
    QVERIFY(!ok);

    QCOMPARE(mapper.stringToEnumValue(QLatin1String("BogusScope::BogusEnum"), &ok), -1);
    QVERIFY(!ok);

    QCOMPARE(mapper.stringToEnumValue(QLatin1String("Garbage"), &ok), -1);
    QVERIFY(!ok);
}

void BrowserTestsTest::testPermissions()
{
    KioskPermissions permissions;
    permissions.setPermission(QLatin1String("WebAttribute::XSSAuditingEnabled"), true);
    QVERIFY(permissions.permission(QWebEngineSettings::XSSAuditingEnabled));

    permissions.setPermission(QLatin1String("WebAttribute::BogusEnum"), true);
    QVERIFY(!permissions.permission(QWebEngineSettings::AutoLoadImages, false));
}

void BrowserTestsTest::testMimeMatchPermissions()
{
    KioskPermissions permissions;
    QHash<QString, QString> mimeRunners;
    mimeRunners[QLatin1String("application/rdp")] = QLatin1String("mstsc.exe");

    permissions.setDownloadPermission(
        QStringList({QLatin1String("application/rdp"), QLatin1String("text/*")}), mimeRunners);

    QVERIFY(permissions.downloadPermission(QLatin1String("application/rdp"), false));
    QVERIFY(permissions.downloadPermission(QLatin1String("text/plain"), false));
    QVERIFY(!permissions.downloadPermission(QLatin1String("application/pdf"), false));
    QVERIFY(permissions.downloadRunner(QLatin1String("application/rdp")) == QLatin1String("mstsc.exe"));
}

QTEST_MAIN(BrowserTestsTest)

#include "tst_browserteststest.moc"
