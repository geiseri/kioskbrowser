#-------------------------------------------------
#
# Project created by QtCreator 2017-02-19T08:23:38
#
#-------------------------------------------------

QT       += widgets webenginewidgets testlib

TARGET = tst_browserteststest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += tst_browserteststest.cpp 

DEFINES += SRCDIR=\\\"$$PWD/\\\"

INCLUDEPATH += "$$top_srcdir/KioskBrowser"

win32:CONFIG(release, debug|release): LIBS += -L$$top_builddir/KioskBrowserCore/release/ -lKioskBrowser
else:win32:CONFIG(debug, debug|release): LIBS += -L$$top_builddir/KioskBrowserCore/debug/ -lKioskBrowser
else:unix:!macx: LIBS += -L$$top_builddir/KioskBrowserCore/ -lKioskBrowser

INCLUDEPATH += $$top_srcdir/KioskBrowserCore
DEPENDPATH += $$top_srcdir/KioskBrowserCore
