/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KIOSKWEBVIEW_H
#define KIOSKWEBVIEW_H

#include "kioskbrowser_export.h"
#include <QWebEngineView>

class KIOSK_CORE_EXPORT KioskWebView : public QWebEngineView
{
    Q_OBJECT
public:
    KioskWebView(QWidget *parent = Q_NULLPTR);
    virtual ~KioskWebView();

    bool isWebActionEnabled(QWebEnginePage::WebAction action);
    void hookupActions();
    void updateActions();

signals:
    void webActionEnabledChanged(QWebEnginePage::WebAction webAction, bool enabled);

protected:
    virtual void contextMenuEvent(QContextMenuEvent *event) Q_DECL_OVERRIDE;
    virtual QWebEngineView *createWindow(QWebEnginePage::WebWindowType type) Q_DECL_OVERRIDE;
};

#endif // KIOSKWEBVIEW_H
