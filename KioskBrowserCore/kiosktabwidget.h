/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KIOSKTABWIDGET_H
#define KIOSKTABWIDGET_H

#include "kioskbrowser_export.h"
#include <QTabWidget>
#include <QWebEnginePage>

class KioskWebView;

class KIOSK_CORE_EXPORT KioskTabWidget : public QTabWidget
{
    Q_OBJECT

public:
    explicit KioskTabWidget(QWidget *parent = 0);
    ~KioskTabWidget();

    KioskWebView *createTab(bool makeCurrent = true);
    KioskWebView *currentWebView() const;
    KioskWebView *webView(int index) const;
    void closeTab(int index);

    QUrl url() const;
    void setUrl(const QUrl &url);

signals:

    void urlChanged(const QUrl &url);
    void titleChanged(const QString &title);
    void loadProgress(int progress);
    void linkHovered(const QString &url);
    void iconChanged(const QIcon &icon);
    void webActionEnabledChanged(QWebEnginePage::WebAction action, bool enabled);

public slots:
    void triggerWebPageAction(QWebEnginePage::WebAction action);

private slots:
    void handleCurrentChanged(int index);

private:
    void setupView(KioskWebView *view, int index);
};

#endif // KIOSKTABWIDGET_H
