/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "kiosktabwidget.h"
#include "kioskbrowser.h"
#include "kioskwebpage.h"
#include "kioskwebview.h"

#include <QTabBar>
#include <QWebEngineProfile>

KioskTabWidget::KioskTabWidget(QWidget *parent) : QTabWidget(parent)
{
    connect(this, &QTabWidget::tabCloseRequested, [this](int index) {
        KioskWebView *view = webView(index);
        if (view) {
            view->page()->triggerAction(QWebEnginePage::RequestClose);
        }
    });

    connect(this, &QTabWidget::currentChanged, this, &KioskTabWidget::handleCurrentChanged);

    tabBar()->setSelectionBehaviorOnRemove(QTabBar::SelectPreviousTab);
    tabBar()->setMovable(true);

    if (KioskBrowser::instance()->hasPermission(KioskPermissions::AllowCreateTabs)) {
        setTabBarAutoHide(false);
        connect(tabBar(), &QTabBar::tabBarDoubleClicked, [this](int index) {
            if (index != -1)
                return;
            createTab();
        });
    } else {
        setTabBarAutoHide(true);
    }
    setDocumentMode(true);
    setElideMode(Qt::ElideMiddle);
    setMovable(false);
}

KioskTabWidget::~KioskTabWidget()
{
    qDebug() << Q_FUNC_INFO;
}

KioskWebView *KioskTabWidget::createTab(bool makeCurrent)
{
    KioskWebView *view = new KioskWebView(this);
    KioskWebPage *page = new KioskWebPage(view);
    view->setPage(page);
    view->hookupActions();

    int tabIndex = insertTab(currentIndex(), view, tr("(Untitled)"));
    if (makeCurrent) {
        setCurrentWidget(view);
    }

    setTabsClosable(count() > 1);

    setupView(view, tabIndex);

    return view;
}

KioskWebView *KioskTabWidget::currentWebView() const
{
    return webView(currentIndex());
}

KioskWebView *KioskTabWidget::webView(int index) const
{
    return qobject_cast<KioskWebView *>(widget(index));
}

void KioskTabWidget::closeTab(int index)
{
    if (KioskWebView *view = webView(index)) {
        bool hasFocus = view->hasFocus();
        removeTab(index);
        if (hasFocus && count() > 0)
            currentWebView()->setFocus();
        if (count() == 0)
            createTab();
        view->deleteLater();
    }
    setTabsClosable(count() > 1);
}

void KioskTabWidget::setupView(KioskWebView *view, int index)
{
    QWebEnginePage *page = view->page();

    connect(view, &QWebEngineView::titleChanged, [this, view, index](const QString &title) {
        if (index != -1)
            setTabText(index, title);
        if (currentIndex() == index)
            emit titleChanged(title);
    });
    connect(view, &QWebEngineView::urlChanged, [this, view, index](const QUrl &url) {
        if (index != -1)
            tabBar()->setTabData(index, url);
        if (currentIndex() == index)
            emit urlChanged(url);
    });
    connect(view, &QWebEngineView::loadProgress, [this, view, index](int progress) {
        if (currentIndex() == index)
            emit loadProgress(progress);
    });
    connect(page, &QWebEnginePage::linkHovered, [this, view, index](const QString &url) {
        if (currentIndex() == index)
            emit linkHovered(url);
    });
    connect(page, &KioskWebPage::iconChanged, [this, view, index](const QIcon &icon) {
        QIcon ico = icon.isNull() ? QIcon(QLatin1String(":defaulticon.png")) : icon;

        if (index != -1)
            setTabIcon(index, ico);
        if (currentIndex() == index)
            emit iconChanged(ico);
    });
    connect(view, &KioskWebView::webActionEnabledChanged,
        [this, view, index](QWebEnginePage::WebAction action, bool enabled) {
            if (currentIndex() == index) {
                bool canEnable = KioskBrowser::instance()->hasPermission(url(), action);
                emit webActionEnabledChanged(action, enabled && canEnable);
            }
        });
    connect(view, &QWebEngineView::loadStarted, [this, view, index]() {
        if (index != -1) {
            QIcon icon(QLatin1String(":view-refresh.png"));
            setTabIcon(index, icon);
        }
    });
    connect(page, &QWebEnginePage::windowCloseRequested, [this, view, index]() {
        if (index >= 0) {
            closeTab(index);
        }
    });
}

void KioskTabWidget::handleCurrentChanged(int index)
{
    if (index != -1) {
        KioskWebView *view = webView(index);
        if (!view->url().isEmpty())
            view->setFocus();
        emit titleChanged(view->title());
        emit urlChanged(view->url());
        QIcon pageIcon = view->page()->icon();
        if (!pageIcon.isNull())
            emit iconChanged(pageIcon);
        else
            emit iconChanged(QIcon(QLatin1String(":defaulticon.png")));
        view->updateActions();
    } else {
        emit titleChanged(QString());
        emit loadProgress(0);
        emit urlChanged(QUrl());
        emit iconChanged(QIcon(QLatin1String(":defaulticon.png")));
        for (int idx = 0; idx < (int)QWebEnginePage::WebActionCount; idx++) {
            emit webActionEnabledChanged((QWebEnginePage::WebAction)idx, false);
        }
    }
}

void KioskTabWidget::setUrl(const QUrl &url)
{
    if (KioskWebView *view = currentWebView()) {
        view->setUrl(url);
        view->setFocus();
    }
}

QUrl KioskTabWidget::url() const
{
    if (KioskWebView *view = currentWebView()) {
        return view->url();
    } else {
        return QUrl();
    }
}

void KioskTabWidget::triggerWebPageAction(QWebEnginePage::WebAction action)
{
    if (KioskWebView *view = currentWebView()) {
        if (KioskBrowser::instance()->hasPermission(view->url(), action)) {
            view->triggerPageAction(action);
            view->setFocus();
        }
    }
}
