/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "kioskminiwindow.h"
#include "kioskwebpage.h"

#include <QIcon>

KioskMiniWindow::KioskMiniWindow(QWidget *parent) : KioskWebView(parent)
{
    setAttribute(Qt::WA_DeleteOnClose);
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

    connect(this, &KioskWebView::titleChanged, this, &QWidget::setWindowTitle);
    connect(this->page(), &KioskWebPage::iconChanged, this, &KioskMiniWindow::setWindowIcon);
    connect(
        this->page(), &KioskWebPage::geometryChangeRequested, this, &KioskMiniWindow::handleGeometryChangeRequested);
    connect(this->page(), &KioskWebPage::windowCloseRequested, this, &QWidget::close);
}

void KioskMiniWindow::handleGeometryChangeRequested(const QRect &newGeometry)
{
    setMinimumSize(newGeometry.width(), newGeometry.height());
    move(newGeometry.topLeft() - pos());
    resize(0, 0);
    show();
}
