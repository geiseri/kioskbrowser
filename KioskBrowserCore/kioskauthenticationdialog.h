/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KIOSKAUTHENTICATIONDIALOG_H
#define KIOSKAUTHENTICATIONDIALOG_H

#include "kioskbrowser_export.h"
#include <QDialog>

namespace Ui
{
class KioskAuthenticationDialog;
}

class KIOSK_CORE_EXPORT KioskAuthenticationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit KioskAuthenticationDialog();
    ~KioskAuthenticationDialog();

    QString username() const;
    QString password() const;

private:
    Ui::KioskAuthenticationDialog *ui;
};

#endif // KIOSKAUTHENTICATIONDIALOG_H
