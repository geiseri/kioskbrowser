/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KIOSKRUNNABLEDOWNLOADITEM_H
#define KIOSKRUNNABLEDOWNLOADITEM_H

#include "kioskbrowser_export.h"
#include "kioskdownloaditem.h"
#include <QObject>

class QTemporaryFile;

class KIOSK_CORE_EXPORT KioskRunnableDownloadItem : public KioskDownloadItem
{
    Q_OBJECT
public:
    explicit KioskRunnableDownloadItem(QWebEngineDownloadItem *download, const QString &handler);

    virtual ~KioskRunnableDownloadItem();

private:
    QTemporaryFile *m_saveFile;
    QString m_handler;

protected:
    virtual void handleFinishedInternal(QWebEngineDownloadItem *download) Q_DECL_OVERRIDE;
    virtual bool setDownloadPathInternal(QWebEngineDownloadItem *download) const Q_DECL_OVERRIDE;
};

#endif // KIOSKRUNNABLEDOWNLOADITEM_H
