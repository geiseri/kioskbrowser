/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KIOSKENUMMAPPER_H
#define KIOSKENUMMAPPER_H

#include "kioskbrowser.h"
#include "kioskbrowser_export.h"
#include <QNetworkProxy>
#include <QObject>
#include <QWebEngineCertificateError>
#include <QWebEnginePage>
#include <QWebEngineSettings>

/*
 * WARNING:  KEEP THIS IN SYNC WITH UPSTREAM ENUMS!
 */
class KIOSK_CORE_EXPORT KioskEnumMapper : public QObject
{
    Q_OBJECT
    Q_ENUMS(BrowserFeature WebAction WebFeature SSLError WebAttribute ProxyType)

public:
    enum BrowserFeature {
        AllowBackButton = 0,
        AllowReloadButton,
        AllowUrlEdit,
        AllowPopups,
        AllowBrowserClose,
        AllowCreateTabs,
        AllowFullscreen,
        AllowUploads,
        AllowDownloads,
        AllowVisitingUrl,
    };

    enum WebAction {
        NoWebAction = -1,
        Back,
        Forward,
        Stop,
        Reload,
        Cut,
        Copy,
        Paste,
        Undo,
        Redo,
        SelectAll,
        ReloadAndBypassCache,
        PasteAndMatchStyle,
        OpenLinkInThisWindow,
        OpenLinkInNewWindow,
        OpenLinkInNewTab,
        CopyLinkToClipboard,
        DownloadLinkToDisk,
        CopyImageToClipboard,
        CopyImageUrlToClipboard,
        DownloadImageToDisk,
        CopyMediaUrlToClipboard,
        ToggleMediaControls,
        ToggleMediaLoop,
        ToggleMediaPlayPause,
        ToggleMediaMute,
        DownloadMediaToDisk,
        InspectElement,
        ExitFullScreen,
        RequestClose,
        Unselect,
        SavePage,
        OpenLinkInNewBackgroundTab,
        ViewSource,
        WebActionCount
    };

    enum WebFeature {
        Notifications = 0,
        Geolocation = 1,
        MediaAudioCapture = 2,
        MediaVideoCapture,
        MediaAudioVideoCapture,
        MouseLock
    };

    enum SSLError {
        SslPinnedKeyNotInCertificateChain = -150,
        CertificateCommonNameInvalid = -200,
        CertificateDateInvalid = -201,
        CertificateAuthorityInvalid = -202,
        CertificateContainsErrors = -203,
        CertificateNoRevocationMechanism = -204,
        CertificateUnableToCheckRevocation = -205,
        CertificateRevoked = -206,
        CertificateInvalid = -207,
        CertificateWeakSignatureAlgorithm = -208,
        CertificateNonUniqueName = -210,
        CertificateWeakKey = -211,
        CertificateNameConstraintViolation = -212,
        CertificateValidityTooLong = -213,
        CertificateTransparencyRequired = -214,
    };

    enum WebAttribute {
        AutoLoadImages,
        JavascriptEnabled,
        JavascriptCanOpenWindows,
        JavascriptCanAccessClipboard,
        LinksIncludedInFocusChain,
        LocalStorageEnabled,
        LocalContentCanAccessRemoteUrls,
        XSSAuditingEnabled,
        SpatialNavigationEnabled,
        LocalContentCanAccessFileUrls,
        HyperlinkAuditingEnabled,
        ScrollAnimatorEnabled,
        ErrorPageEnabled,
        PluginsEnabled,
        FullScreenSupportEnabled,
        ScreenCaptureEnabled,
        WebGLEnabled,
        Accelerated2dCanvasEnabled,
        AutoLoadIconsForPage,
        TouchIconsEnabled,
        FocusOnNavigationEnabled,
        PrintElementBackgrounds,
        AllowRunningInsecureContent,
        AllowGeolocationOnInsecureOrigins
    };

    enum ProxyType { DefaultProxy, Socks5Proxy, NoProxy, HttpProxy, HttpCachingProxy, FtpCachingProxy };

    explicit KioskEnumMapper(QObject *parent = 0);

    int stringToEnumValue(const QString &name, bool *ok = 0) const;
    QStringList enumNames() const;

signals:

public slots:
};

#endif // KIOSKENUMMAPPER_H
