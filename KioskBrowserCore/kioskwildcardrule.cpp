/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "kioskwildcardrule.h"
#include <QDebug>

KioskWildcardRule::KioskWildcardRule(const QString &regexp) : m_expression(regexp)
{
    m_expression.setPatternSyntax(QRegExp::WildcardUnix);
}

bool KioskWildcardRule::isMatch(const QUrl &url) const
{
    if (!m_expression.isValid()) {
        qDebug() << Q_FUNC_INFO << m_expression.errorString();
        return false;
    }
    QString urlString = url.toString(QUrl::PrettyDecoded | QUrl::RemoveQuery | QUrl::RemoveFragment);
    if (m_expression.exactMatch(urlString)) {
        return true;
    } else {
        return false;
    }

    return false;
}

bool KioskWildcardRule::isValid() const
{
    return m_expression.isValid();
}
