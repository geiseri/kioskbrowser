/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KIOSKUTILITIES_H
#define KIOSKUTILITIES_H

#include "kioskbrowser_export.h"
#include <QUrl>

class KIOSK_CORE_EXPORT KioskUtilities
{
public:
    KioskUtilities();

    static QUrl fixupUrlInput(const QString &urlText);
    static QUrl searchEngineQuery(const QString &engine, const QString &urlText);
};

#endif // KIOSKUTILITIES_H
