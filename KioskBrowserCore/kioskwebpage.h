/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KIOSKWEBPAGE_H
#define KIOSKWEBPAGE_H

#include "kioskbrowser_export.h"
#include <QWebEngineFullScreenRequest>
#include <QWebEnginePage>

class KIOSK_CORE_EXPORT KioskWebPage : public QWebEnginePage
{
    Q_OBJECT
public:
    explicit KioskWebPage(QObject *parent = Q_NULLPTR);
    virtual ~KioskWebPage();

public:
    virtual void triggerAction(WebAction action, bool checked = false) Q_DECL_OVERRIDE;

signals:
    void actionEnabledChanged(WebAction action, bool state);

private slots:
    void handleFeaturePermissionRequested(const QUrl &securityOrigin, QWebEnginePage::Feature feature);
    void handleFullScreenRequested(QWebEngineFullScreenRequest fullScreenRequest);
    void handleAuthenticationRequired(const QUrl &requestUrl, QAuthenticator *authenticator);
    void handleProxyAuthenticationRequired(
        const QUrl &requestUrl, QAuthenticator *authenticator, const QString &proxyHost);
    void handleUrlChanged(const QUrl &newUrl);

protected:
    virtual QStringList chooseFiles(
        FileSelectionMode mode, const QStringList &oldFiles, const QStringList &acceptedMimeTypes) Q_DECL_OVERRIDE;
    virtual void javaScriptAlert(const QUrl &securityOrigin, const QString &msg) Q_DECL_OVERRIDE;
    virtual bool javaScriptConfirm(const QUrl &securityOrigin, const QString &msg) Q_DECL_OVERRIDE;
    virtual bool javaScriptPrompt(
        const QUrl &securityOrigin, const QString &msg, const QString &defaultValue, QString *result) Q_DECL_OVERRIDE;
    virtual void javaScriptConsoleMessage(JavaScriptConsoleMessageLevel level, const QString &message, int lineNumber,
        const QString &sourceID) Q_DECL_OVERRIDE;
    virtual bool certificateError(const QWebEngineCertificateError &certificateError) Q_DECL_OVERRIDE;
    virtual bool acceptNavigationRequest(const QUrl &url, NavigationType type, bool isMainFrame) Q_DECL_OVERRIDE;
};

#endif // KIOSKWEBPAGE_H
