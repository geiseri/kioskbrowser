/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "kioskbrowser.h"
#include "kioskbrowsermainwindow.h"
#include "kioskdownloaditem.h"
#include "kioskenummapper.h"
#include "kioskrunnabledownloaditem.h"

#include <QApplication>
#include <QAuthenticator>
#include <QDir>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkProxy>
#include <QTemporaryDir>
#include <QWebEngineProfile>

Q_GLOBAL_STATIC(KioskBrowser, kioskBrowser)

KioskBrowser::~KioskBrowser()
{
    qDebug() << Q_FUNC_INFO;
    delete m_storagePath;
}

QVector<KioskBrowserMainWindow *> KioskBrowser::windows() const
{
    return m_windows;
}

void KioskBrowser::addWindow(KioskBrowserMainWindow *window)
{
    if (m_windows.contains(window))
        return;

    m_windows.prepend(window);
    QObject::connect(window, &QObject::destroyed, [this, window]() {
        m_windows.removeOne(window);
        // qDebug() << "Window count" << m_windows.count();
        // if( m_windows.count() == 0 ) {
        //    QCoreApplication::exit(0);
        //}
    });
    window->show();
}

bool KioskBrowser::hasPermission(KioskPermissions::BrowserFeature feature) const
{
    return m_globalPermissions->permission(feature, m_usePermissionsWhitelist);
}

bool KioskBrowser::hasPermission(const QUrl &url, KioskPermissions::BrowserFeature feature) const
{
    for (QPair<KioskWildcardRule, KioskPermissions *> rule : m_permissions) {
        if (rule.first.isMatch(url)) {
            return rule.second->permission(feature, m_usePermissionsWhitelist);
        }
    }
    return m_usePermissionsWhitelist;
}

bool KioskBrowser::hasPermission(const QUrl &url, QWebEnginePage::WebAction action) const
{
    for (QPair<KioskWildcardRule, KioskPermissions *> rule : m_permissions) {
        if (rule.first.isMatch(url)) {
            return rule.second->permission(action, m_usePermissionsWhitelist);
        }
    }
    return m_usePermissionsWhitelist;
}

bool KioskBrowser::hasPermission(const QUrl &url, QWebEnginePage::Feature feature) const
{
    for (QPair<KioskWildcardRule, KioskPermissions *> rule : m_permissions) {
        if (rule.first.isMatch(url)) {
            return rule.second->permission(feature, m_usePermissionsWhitelist);
        }
    }
    return m_usePermissionsWhitelist;
}

void KioskBrowser::configurePage(const QUrl &url, QWebEngineSettings *settings) const
{
    m_globalPermissions->configureSettings(settings);

    for (QPair<KioskWildcardRule, KioskPermissions *> rule : m_permissions) {
        if (rule.first.isMatch(url)) {
            rule.second->configureSettings(settings);
        }
    }
}

bool KioskBrowser::canIgnoreSslError(const QUrl &url, QWebEngineCertificateError::Error error) const
{
    for (QPair<KioskWildcardRule, KioskPermissions *> rule : m_permissions) {
        if (rule.first.isMatch(url)) {
            return rule.second->permission(error, m_usePermissionsWhitelist);
        }
    }
    return m_usePermissionsWhitelist;
}

bool KioskBrowser::canDownloadMimeType(const QUrl &url, const QString &mimeType) const
{
    for (QPair<KioskWildcardRule, KioskPermissions *> rule : m_permissions) {
        if (rule.first.isMatch(url)) {
            return rule.second->downloadPermission(mimeType, m_usePermissionsWhitelist);
        }
    }
    return m_usePermissionsWhitelist;
}

QUrl KioskBrowser::startUrl() const
{
    return m_startUrl;
}

QString KioskBrowser::downloadPath() const
{
    return m_downloadPath;
}

QString KioskBrowser::mimeHandler(const QUrl &url, const QString &mimeType) const
{
    for (QPair<KioskWildcardRule, KioskPermissions *> rule : m_permissions) {
        if (rule.first.isMatch(url)) {
            return rule.second->downloadRunner(mimeType);
        }
    }
    return m_globalPermissions->downloadRunner(mimeType);
}

bool KioskBrowser::configureBrowser(const QString &configFile)
{
    // configure m_profile with settings from json file
    // permissions from json file

    QFile loadFile(configFile);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning() << "Couldn't open configuration file." << configFile;
        return false;
    }

    QByteArray configurationData = loadFile.readAll();

    QJsonParseError err;
    QJsonObject configuration = QJsonDocument::fromJson(configurationData, &err).object();
    if (err.error != QJsonParseError::NoError) {
        qWarning() << "Couldn't parse configuration file." << err.errorString();
        return false;
    }

    configureBrowserSettings(configuration.value("GlobalSettings").toObject());
    configureProxySettings(configuration.value("GlobalSettings").toObject());
    configureBrowserPermissions(configuration.value("Sites").toArray());

    return true;
}

KioskBrowser *KioskBrowser::instance()
{
    return kioskBrowser;
}

void KioskBrowser::configureProxySettings(const QJsonObject &config)
{
    if (!config.value("Proxy").isUndefined()) {
        QJsonObject proxyConfig = config.value("Proxy").toObject();

        QNetworkProxy proxy;
        proxy.setType((QNetworkProxy::ProxyType)m_enumMapper->stringToEnumValue(
            proxyConfig.value("ProxyType").toString(QLatin1String("ProxyType::NoProxy"))));
        proxy.setHostName(proxyConfig.value("HostName").toString());
        proxy.setPort(proxyConfig.value("Port").toInt());
        QNetworkProxy::setApplicationProxy(proxy);
    }
}

void KioskBrowser::configureBrowserSettings(const QJsonObject &config)
{
    m_startUrl = QUrl("http://www.google.com");
    m_searchEngine = QLatin1String("google");
    m_downloadPath = QDir::tempPath();

    if (!config.value("Browser").isUndefined()) {
        QJsonObject browserConfig = config.value("Browser").toObject();

        m_startUrl = QUrl(browserConfig.value("StartUrl").toString(m_startUrl.toString()));
        m_downloadPath = browserConfig.value("DownloadPath").toString(m_downloadPath);
        m_searchEngine = browserConfig.value("SearchEngine").toString(m_searchEngine);
        m_usePermissionsWhitelist = browserConfig.value("WhitelistPermissions").toBool(false);
        configurePermissions(m_globalPermissions, config);
        configureDownloadMimetypes(m_globalPermissions, config.value("MimeTypes").toArray());
    }
}

void KioskBrowser::configurePermissions(KioskPermissions *permissionList, const QJsonObject &permissions)
{
    for (QString permission : permissions.value("Access").toObject().keys()) {
        bool access = permissions.value("Access").toObject().value(permission).toBool();
        permissionList->setPermission(permission, access);
    }
}

void KioskBrowser::configureBrowserPermissions(const QJsonArray &permissions)
{
    for (QJsonValue value : permissions) {
        KioskWildcardRule rule = KioskWildcardRule(value.toObject().value("Match").toString());
        if (rule.isValid()) {
            KioskPermissions *permissionList = new KioskPermissions(this);
            configurePermissions(permissionList, value.toObject());
            configureDownloadMimetypes(permissionList, value.toObject().value("MimeTypes").toArray());
            m_permissions << QPair<KioskWildcardRule, KioskPermissions *>(rule, permissionList);
        }
    }
}

void KioskBrowser::configureDownloadMimetypes(KioskPermissions *permissionsList, const QJsonArray &mimetypes)
{
    QStringList mimeTypeList;
    QHash<QString, QString> mimeMap;
    for (QJsonValue mimeType : mimetypes) {
        QJsonObject mimeTypeObject = mimeType.toObject();
        mimeTypeList << mimeTypeObject[QLatin1String("Type")].toString();
        if (mimeTypeObject.contains(QLatin1String("Runner"))) {
            mimeMap[mimeTypeObject[QLatin1String("Type")].toString()]
                = mimeTypeObject[QLatin1String("Runner")].toString();
        }
    }

    permissionsList->setDownloadPermission(mimeTypeList, mimeMap);
}

QString KioskBrowser::searchEngine() const
{
    return m_searchEngine;
}

QWebEngineProfile *KioskBrowser::profile() const
{
    return m_profile;
}

void KioskBrowser::cleanup()
{
    for (KioskBrowserMainWindow *w : m_windows) {
        m_windows.removeOne(w);
        delete w;
    }

    delete m_profile;

    qDebug() << Q_FUNC_INFO << "cleanup browser";
}

KioskBrowser::KioskBrowser(QObject *parent) : QObject(parent), m_usePermissionsWhitelist(false)
{
    m_storagePath = new QTemporaryDir;
    m_profile = new QWebEngineProfile(QLatin1String("KioskBrowser"), this);
    m_profile->setPersistentStoragePath(m_storagePath->path());

    m_globalPermissions = new KioskPermissions(this);

    m_enumMapper = new KioskEnumMapper(this);

    connect(m_profile, &QWebEngineProfile::downloadRequested, [this](QWebEngineDownloadItem *download) {
        if (hasPermission(download->url(), KioskPermissions::AllowDownloads)
            && canDownloadMimeType(download->url(), download->mimeType())) {

            KioskDownloadItem *item = 0;
            if (mimeHandler(download->url(), download->mimeType()).isEmpty()) {
                item = new KioskDownloadItem(download);
            } else {
                item = new KioskRunnableDownloadItem(download, mimeHandler(download->url(), download->mimeType()));
            }

            if (item->setDownloadPath()) {
                download->accept();
            } else {
                download->cancel();
            }
        }
    });
}
