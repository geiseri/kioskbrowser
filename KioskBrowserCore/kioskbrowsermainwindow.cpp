/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "kioskbrowsermainwindow.h"
#include "kioskbrowser.h"
#include "kiosktabwidget.h"
#include "kioskutilities.h"
#include "kioskwebview.h"

#include "ui_kioskbrowsermainwindow.h"

#include <QCloseEvent>
#include <QLineEdit>
#include <QStatusBar>
#include <QToolBar>

KioskBrowserMainWindow::KioskBrowserMainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::KioskBrowserMainWindow)
{
    qDebug() << Q_FUNC_INFO;

    setAttribute(Qt::WA_DeleteOnClose);
    ui->setupUi(this);

    if (KioskBrowser::instance()->hasPermission(KioskPermissions::AllowBackButton)) {
        QAction *backAction
            = ui->mainToolBar->addAction(QIcon::fromTheme(QLatin1String("arrow-left"), QIcon(":/icons/back.svg")),
                tr("Back"), [this] { currentTab()->back(); });

        connect(ui->tabWidget, &KioskTabWidget::webActionEnabledChanged,
            [this, backAction](QWebEnginePage::WebAction action, bool enabled) {
                if (action == QWebEnginePage::Back) {
                    backAction->setEnabled(enabled);
                }
            });
    }

    if (KioskBrowser::instance()->hasPermission(KioskPermissions::AllowReloadButton)) {
        QAction *reloadAction
            = ui->mainToolBar->addAction(QIcon::fromTheme(QLatin1String("reload"), QIcon(":/icons/reload.svg")),
                tr("Reload"), [this] { currentTab()->reload(); });

        connect(ui->tabWidget, &KioskTabWidget::webActionEnabledChanged,
            [this, reloadAction](QWebEnginePage::WebAction action, bool enabled) {
                if (action == QWebEnginePage::Reload) {
                    reloadAction->setEnabled(enabled);
                }

            });
    }

    if (KioskBrowser::instance()->hasPermission(KioskPermissions::AllowUrlEdit)) {
        QLineEdit *urlEdit = new QLineEdit;
        connect(urlEdit, &QLineEdit::editingFinished,
            [this, urlEdit]() { ui->tabWidget->setUrl(KioskUtilities::fixupUrlInput(urlEdit->text())); });
        connect(ui->tabWidget, &KioskTabWidget::urlChanged,
            [this, urlEdit](const QUrl &url) { urlEdit->setText(url.toDisplayString()); });

        ui->mainToolBar->addWidget(urlEdit);
    }

    connect(ui->tabWidget, &KioskTabWidget::iconChanged, this, &KioskBrowserMainWindow::setWindowIcon);
    connect(ui->tabWidget, &KioskTabWidget::titleChanged, this, &KioskBrowserMainWindow::setWindowTitle);
    connect(ui->tabWidget, &KioskTabWidget::linkHovered,
        [this](const QString &url) { ui->statusBar->showMessage(url, 5000); });

    ui->tabWidget->createTab()->setUrl(KioskBrowser::instance()->startUrl());
}

KioskBrowserMainWindow::~KioskBrowserMainWindow()
{
    qDebug() << Q_FUNC_INFO;
    delete ui;
}

void KioskBrowserMainWindow::closeEvent(QCloseEvent *event)
{
    if (KioskBrowser::instance()->hasPermission(KioskPermissions::AllowBrowserClose)) {
        event->accept();
    } else {
        event->ignore();
    }
}

KioskTabWidget *KioskBrowserMainWindow::tabWidget() const
{
    return ui->tabWidget;
}

KioskWebView *KioskBrowserMainWindow::currentTab() const
{
    return qobject_cast<KioskWebView *>(ui->tabWidget->currentWidget());
}

void KioskBrowserMainWindow::on_actionClose_triggered()
{
    close();
}

void KioskBrowserMainWindow::on_actionCut_triggered()
{
    currentTab()->triggerPageAction(QWebEnginePage::Cut);
}

void KioskBrowserMainWindow::on_actionCopy_triggered()
{
    currentTab()->triggerPageAction(QWebEnginePage::Copy);
}

void KioskBrowserMainWindow::on_actionPaste_triggered()
{
    currentTab()->triggerPageAction(QWebEnginePage::Paste);
}
