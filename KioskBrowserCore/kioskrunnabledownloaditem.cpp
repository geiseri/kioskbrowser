/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "kioskrunnabledownloaditem.h"
#include "kioskbrowser.h"
#include <QCoreApplication>
#include <QFileInfo>
#include <QProcess>
#include <QTemporaryFile>

KioskRunnableDownloadItem::KioskRunnableDownloadItem(QWebEngineDownloadItem *download, const QString &handler)
    : KioskDownloadItem(download), m_saveFile(new QTemporaryFile(this)), m_handler(handler)
{
}

KioskRunnableDownloadItem::~KioskRunnableDownloadItem()
{
}

void KioskRunnableDownloadItem::handleFinishedInternal(QWebEngineDownloadItem *download)
{
    QProcess *runner = new QProcess(this);
    connect(runner, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
        [this, download](int exitCode, QProcess::ExitStatus status) {
            if (status == QProcess::CrashExit) {
                qDebug() << Q_FUNC_INFO << m_handler << "Crashed!";
            }
            qDebug() << Q_FUNC_INFO << m_handler << exitCode;
            KioskDownloadItem::handleFinishedInternal(download);
        });
    connect(runner, &QProcess::errorOccurred, [this, download](QProcess::ProcessError error) {
        qDebug() << Q_FUNC_INFO << "Error" << error;
        KioskDownloadItem::handleFinishedInternal(download);
    });

    runner->start(m_handler, QStringList({m_saveFile->fileName()}));
    qDebug() << Q_FUNC_INFO << "start";
}

bool KioskRunnableDownloadItem::setDownloadPathInternal(QWebEngineDownloadItem *download) const
{
    QFileInfo downloadFileInfo(download->path());
    m_saveFile->setFileTemplate(QLatin1String("kioskbrowser-XXXXXX.") + downloadFileInfo.suffix());
    if (m_saveFile->open()) {
        m_saveFile->close();
    } else {
        return false;
    }

    download->setPath(m_saveFile->fileName());
    return true;
}
