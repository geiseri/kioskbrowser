/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "kioskwebview.h"
#include "kioskbrowser.h"
#include "kioskbrowsermainwindow.h"
#include "kioskminiwindow.h"
#include "kiosktabwidget.h"
#include "kioskwebpage.h"

#include <QAction>

KioskWebView::KioskWebView(QWidget *parent) : QWebEngineView(parent)
{
    qDebug() << Q_FUNC_INFO << "view created";
}

KioskWebView::~KioskWebView()
{
    qDebug() << Q_FUNC_INFO << "view destroyed" << url();
}

bool KioskWebView::isWebActionEnabled(QWebEnginePage::WebAction action)
{
    return KioskBrowser::instance()->hasPermission(url(), action) && page()->action(action)->isEnabled();
}

void KioskWebView::updateActions()
{
    for (int idx = 0; idx < (int)QWebEnginePage::WebActionCount; idx++) {
        emit webActionEnabledChanged(
            (QWebEnginePage::WebAction)idx, isWebActionEnabled((QWebEnginePage::WebAction)idx));
    }
}

void KioskWebView::hookupActions()
{
    for (int idx = 0; idx < (int)QWebEnginePage::WebActionCount; idx++) {
        connect(page()->action((QWebEnginePage::WebAction)idx), &QAction::changed, [this, idx] {
            emit webActionEnabledChanged(
                (QWebEnginePage::WebAction)idx, isWebActionEnabled((QWebEnginePage::WebAction)idx));
        });
    }
}

void KioskWebView::contextMenuEvent(QContextMenuEvent *event)
{
    QWebEngineView::contextMenuEvent(event);
}

QWebEngineView *KioskWebView::createWindow(QWebEnginePage::WebWindowType type)
{
    QWebEngineView *view = 0;
    switch (type) {
    case QWebEnginePage::WebBrowserTab: {
        KioskBrowserMainWindow *mainWindow = qobject_cast<KioskBrowserMainWindow *>(window());
        view = mainWindow->tabWidget()->createTab();
        break;
    }
    case QWebEnginePage::WebBrowserBackgroundTab: {
        KioskBrowserMainWindow *mainWindow = qobject_cast<KioskBrowserMainWindow *>(window());
        view = mainWindow->tabWidget()->createTab(false);
        break;
    }
    case QWebEnginePage::WebBrowserWindow: {
        KioskBrowserMainWindow *mainWindow = new KioskBrowserMainWindow;
        KioskBrowser::instance()->addWindow(mainWindow);
        view = mainWindow->currentTab();
        break;
    }
    case QWebEnginePage::WebDialog: {
        if (KioskBrowser::instance()->hasPermission(url(), KioskPermissions::AllowPopups)) {
            view = new KioskMiniWindow();
        }
        break;
    }
    }
    return view;
}
