/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "kioskutilities.h"
#include "kioskbrowser.h"
#include <QUrlQuery>

KioskUtilities::KioskUtilities()
{
}

QUrl KioskUtilities::fixupUrlInput(const QString &urlText)
{

    QUrl url = QUrl::fromUserInput(urlText);
    if (url.isValid()) {
        return url;
    }

    return searchEngineQuery(KioskBrowser::instance()->searchEngine(), urlText);
}

QUrl KioskUtilities::searchEngineQuery(const QString &engine, const QString &urlText)
{

    // TODO: make this configurable
    QUrl url = QUrl("http://www.google.com/search");
    QUrlQuery query;
    query.addQueryItem("q", QUrl::toPercentEncoding(urlText));
    url.setQuery(query);
    return url;
}
