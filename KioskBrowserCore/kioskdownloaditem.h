/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KIOSKDOWNLOADITEM_H
#define KIOSKDOWNLOADITEM_H

#include "kioskbrowser_export.h"
#include <QObject>
#include <QWebEngineDownloadItem>

class KIOSK_CORE_EXPORT KioskDownloadItem : public QObject
{
    Q_OBJECT
public:
    explicit KioskDownloadItem(QWebEngineDownloadItem *download);
    virtual ~KioskDownloadItem();

    virtual bool setDownloadPath();

private slots:
    void handleDownloadProgress(qint64 bytesReceived, qint64 bytesTotal);
    void handleFinished();
    void handleStateChanged(QWebEngineDownloadItem::DownloadState state);

protected:
    virtual bool setDownloadPathInternal(QWebEngineDownloadItem *download) const;
    virtual void handleFinishedInternal(QWebEngineDownloadItem *download);

private:
    QWebEngineDownloadItem *m_download;
};

#endif // KIOSKDOWNLOADITEM_H
