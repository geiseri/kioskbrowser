QT += core gui widgets webenginewidgets

TARGET = KioskBrowser
TEMPLATE = lib

DEFINES += BUILD_KIOSK_CORE KIOSK_CORE_DLL

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    kioskwebpage.cpp \
    kioskwebview.cpp \
    kioskbrowser.cpp \
    kiosktabwidget.cpp \
    kioskutilities.cpp \
    kioskminiwindow.cpp \
    kioskdownloaditem.cpp \
    kioskauthenticationdialog.cpp \
    kioskpermissions.cpp \
    kioskenummapper.cpp \
    kioskwildcardrule.cpp \
    kioskbrowsermainwindow.cpp \
    kioskrunnabledownloaditem.cpp

HEADERS  += \
    kioskwebpage.h \
    kioskwebview.h \
    kioskbrowser.h \
    kiosktabwidget.h \
    kioskutilities.h \
    kioskminiwindow.h \
    kioskdownloaditem.h \
    kioskauthenticationdialog.h \
    kioskpermissions.h \
    kioskenummapper.h \
    kioskwildcardrule.h \
    kioskbrowsermainwindow.h \
    kioskbrowser_export.h \
    kioskrunnabledownloaditem.h

FORMS    += \
    kioskauthenticationdialog.ui \
    kioskbrowsermainwindow.ui

RESOURCES += \
    resources.qrc

INSTALLS += target
target.path = /usr/lib
