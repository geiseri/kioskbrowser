/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KIOSKBROWSER_H
#define KIOSKBROWSER_H

#include <QObject>
#include <QVector>
#include <QWebEngineCertificateError>
#include <QWebEnginePage>
#include <QWebEngineSettings>

#include "kioskbrowser_export.h"
#include "kioskpermissions.h"
#include "kioskwildcardrule.h"

class KioskBrowserMainWindow;
class KioskEnumMapper;
class QWebEngineProfile;
class QTemporaryDir;

class KIOSK_CORE_EXPORT KioskBrowser : public QObject
{
    Q_OBJECT
public:
    explicit KioskBrowser(QObject *parent = 0);
    ~KioskBrowser();

    QVector<KioskBrowserMainWindow *> windows() const;
    void addWindow(KioskBrowserMainWindow *window);

    bool hasPermission(KioskPermissions::BrowserFeature feature) const;
    bool hasPermission(const QUrl &url, KioskPermissions::BrowserFeature feature) const;
    bool hasPermission(const QUrl &url, QWebEnginePage::WebAction action) const;
    bool hasPermission(const QUrl &url, QWebEnginePage::Feature feature) const;
    void configurePage(const QUrl &url, QWebEngineSettings *settings) const;

    bool canIgnoreSslError(const QUrl &url, QWebEngineCertificateError::Error error) const;
    bool canDownloadMimeType(const QUrl &url, const QString &mimeType) const;

    QUrl startUrl() const;
    QString searchEngine() const;
    QString downloadPath() const;
    QString mimeHandler(const QUrl &url, const QString &mimeType) const;

    bool configureBrowser(const QString &configFile);
    QWebEngineProfile *profile() const;

    void cleanup();

    static KioskBrowser *instance();

private:
    void configureProxySettings(const QJsonObject &config);
    void configureBrowserSettings(const QJsonObject &config);
    void configureBrowserPermissions(const QJsonArray &permissions);
    void configurePermissions(KioskPermissions *permissionList, const QJsonObject &permissions);
    void configureDownloadMimetypes(KioskPermissions *permissionsList, const QJsonArray &mimetypes);

private:
    QVector<KioskBrowserMainWindow *> m_windows;
    QWebEngineProfile *m_profile;
    KioskPermissions *m_globalPermissions;
    QVector<QPair<KioskWildcardRule, KioskPermissions *>> m_permissions;
    QUrl m_startUrl;
    QString m_downloadPath;
    QTemporaryDir *m_storagePath;
    KioskEnumMapper *m_enumMapper;
    bool m_usePermissionsWhitelist;
    QString m_searchEngine;
};

#endif // KIOSKBROWSER_H
