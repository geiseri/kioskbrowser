/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "kioskdownloaditem.h"
#include "kioskbrowser.h"
#include <QApplication>
#include <QDebug>
#include <QFileDialog>
#include <QFileInfo>

KioskDownloadItem::KioskDownloadItem(QWebEngineDownloadItem *download) : QObject(download), m_download(download)
{
    connect(m_download, &QWebEngineDownloadItem::downloadProgress, this, &KioskDownloadItem::handleDownloadProgress);
    connect(m_download, &QWebEngineDownloadItem::finished, this, &KioskDownloadItem::handleFinished);
    connect(m_download, &QWebEngineDownloadItem::stateChanged, this, &KioskDownloadItem::handleStateChanged);
}

KioskDownloadItem::~KioskDownloadItem()
{
}

bool KioskDownloadItem::setDownloadPath()
{
    return setDownloadPathInternal(m_download);
}

void KioskDownloadItem::handleDownloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    qDebug() << Q_FUNC_INFO << bytesReceived << bytesTotal;
}

void KioskDownloadItem::handleFinished()
{
    qDebug() << Q_FUNC_INFO << m_download->url() << "to" << m_download->path();
    handleFinishedInternal(m_download);
}

void KioskDownloadItem::handleStateChanged(QWebEngineDownloadItem::DownloadState state)
{
    qDebug() << Q_FUNC_INFO << state;
}

bool KioskDownloadItem::setDownloadPathInternal(QWebEngineDownloadItem *download) const
{
    QFileInfo downloadFileInfo(download->path());
    QString downloadPath = QFileDialog::getSaveFileName(QApplication::activeWindow(), tr("Save file..."),
        KioskBrowser::instance()->downloadPath() + QDir::separator() + downloadFileInfo.fileName(),
        downloadFileInfo.suffix());
    if (downloadPath.isEmpty()) {
        return false;
    }

    download->setPath(downloadPath);
    return true;
}

void KioskDownloadItem::handleFinishedInternal(QWebEngineDownloadItem *download)
{
    download->deleteLater();
}
