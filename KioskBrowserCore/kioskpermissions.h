/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PERMISSIONS_H
#define PERMISSIONS_H

#include "kioskbrowser_export.h"
#include <QNetworkProxy>
#include <QObject>
#include <QWebEngineCertificateError>
#include <QWebEnginePage>
#include <QWebEngineSettings>

class KioskEnumMapper;
class KIOSK_CORE_EXPORT KioskPermissions : public QObject
{
    Q_OBJECT
    Q_ENUMS(BrowserFeature)
public:
    enum BrowserFeature {
        AllowBackButton = 0,
        AllowReloadButton,
        AllowUrlEdit,
        AllowPopups,
        AllowBrowserClose,
        AllowCreateTabs,
        AllowFullscreen,
        AllowUploads,
        AllowDownloads,
        AllowVisitingUrl,
    };

    KioskPermissions(QObject *parent = 0);

    void setPermission(const QString &key, bool value);

    void setPermission(BrowserFeature feature, bool enable = true);
    bool permission(BrowserFeature feature, bool defaultValue = true) const;

    void setPermission(QWebEnginePage::WebAction action, bool enable = true);
    bool permission(QWebEnginePage::WebAction action, bool defaultValue = true) const;

    void setPermission(QWebEnginePage::Feature feature, bool enable = true);
    bool permission(QWebEnginePage::Feature feature, bool defaultValue = true) const;

    void setPermission(QWebEngineCertificateError::Error error, bool enable = true);
    bool permission(QWebEngineCertificateError::Error error, bool defaultValue = true) const;

    void setPermission(QWebEngineSettings::WebAttribute attribute, bool enable = true);
    bool permission(QWebEngineSettings::WebAttribute attribute, bool defaultValue = true) const;

    void setDownloadPermission(const QStringList &mimeTypes, const QHash<QString, QString> &mimeRunners);
    bool downloadPermission(const QString &mimeType, bool defaultValue = true) const;
    QString downloadRunner(const QString &mimeType) const;

    void configureSettings(QWebEngineSettings *settings) const;

private:
    QVector<BrowserFeature> m_browserFeatures;
    QVector<QWebEnginePage::WebAction> m_webActions;
    QVector<QWebEnginePage::Feature> m_webFeatures;
    QVector<QWebEngineCertificateError::Error> m_sslErrors;
    QVector<QWebEngineSettings::WebAttribute> m_webAttributes;
    QVector<BrowserFeature> m_browserFeaturesDisabled;
    QVector<QWebEnginePage::WebAction> m_webActionsDisabled;
    QVector<QWebEnginePage::Feature> m_webFeaturesDisabled;
    QVector<QWebEngineCertificateError::Error> m_sslErrorsDisabled;
    QVector<QWebEngineSettings::WebAttribute> m_webAttributesDisabled;
    QVector<QRegExp> m_acceptableDownloadMimeTypes;
    QVector<QPair<QRegExp, QString>> m_mimeRunners;
    KioskEnumMapper *m_enumMapper;
};

#endif // PERMISSIONS_H
