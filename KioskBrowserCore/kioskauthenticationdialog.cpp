/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "kioskauthenticationdialog.h"
#include "ui_kioskauthenticationdialog.h"
#include <QApplication>
#include <QDialogButtonBox>
#include <QLineEdit>

KioskAuthenticationDialog::KioskAuthenticationDialog()
    : QDialog(QApplication::activeWindow()), ui(new Ui::KioskAuthenticationDialog)
{
    ui->setupUi(this);
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &KioskAuthenticationDialog::accept);
    connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &KioskAuthenticationDialog::reject);
}

KioskAuthenticationDialog::~KioskAuthenticationDialog()
{
    delete ui;
}

QString KioskAuthenticationDialog::username() const
{
    return ui->usernameEdit->text();
}

QString KioskAuthenticationDialog::password() const
{
    return ui->passwordEdit->text();
}
