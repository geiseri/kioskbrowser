/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "kioskpermissions.h"
#include "kioskenummapper.h"

template <typename T, typename X, typename Y>
void enableThing(T thing, X &enabledThings, Y &disabledThings, bool enable)
{
    if (enable) {
        enabledThings.append(thing);
    } else {
        disabledThings.append(thing);
    }
}

template <typename T, typename X, typename Y>
bool isThingEnabled(const T &thing, const X &enabledThings, const Y &disabledThings, bool defaultValue)
{
    bool disabled = disabledThings.contains(thing);
    if (disabled) {
        return false;
    } else {
        return enabledThings.contains(thing) || defaultValue;
    }
}

KioskPermissions::KioskPermissions(QObject *parent) : QObject(parent)
{
    m_enumMapper = new KioskEnumMapper(this);
}

void KioskPermissions::setPermission(const QString &key, bool value)
{
    bool okay = false;
    if (key.startsWith("BrowserFeature")) {
        BrowserFeature enumValue = (KioskPermissions::BrowserFeature)m_enumMapper->stringToEnumValue(key, &okay);
        if (okay) {
            setPermission(enumValue, value);
        }
        return;
    } else if (key.startsWith("WebAction")) {
        QWebEnginePage::WebAction enumValue = (QWebEnginePage::WebAction)m_enumMapper->stringToEnumValue(key, &okay);
        if (okay) {
            setPermission(enumValue, value);
        }
        return;
    } else if (key.startsWith("WebFeature")) {
        QWebEnginePage::Feature enumValue = (QWebEnginePage::Feature)m_enumMapper->stringToEnumValue(key, &okay);
        if (okay) {
            setPermission(enumValue, value);
        }
        return;
    } else if (key.startsWith("SSLError")) {
        QWebEngineCertificateError::Error enumValue
            = (QWebEngineCertificateError::Error)m_enumMapper->stringToEnumValue(key, &okay);
        if (okay) {
            setPermission(enumValue, value);
        }
        return;
    } else if (key.startsWith("WebAttribute")) {
        QWebEngineSettings::WebAttribute enumValue
            = (QWebEngineSettings::WebAttribute)m_enumMapper->stringToEnumValue(key, &okay);
        if (okay) {
            setPermission(enumValue, value);
        }
        return;
    }

    qDebug() << Q_FUNC_INFO << "unknown enum" << key;
}

void KioskPermissions::setPermission(BrowserFeature feature, bool enable)
{
    enableThing(feature, m_browserFeatures, m_browserFeaturesDisabled, enable);
}

bool KioskPermissions::permission(BrowserFeature feature, bool defaultValue) const
{
    return isThingEnabled(feature, m_browserFeatures, m_browserFeaturesDisabled, defaultValue);
}

void KioskPermissions::setPermission(QWebEnginePage::WebAction action, bool enable)
{
    enableThing(action, m_webActions, m_webActionsDisabled, enable);
}

bool KioskPermissions::permission(QWebEnginePage::WebAction action, bool defaultValue) const
{
    return isThingEnabled(action, m_webActions, m_webActionsDisabled, defaultValue);
}

void KioskPermissions::setPermission(QWebEnginePage::Feature feature, bool enable)
{
    enableThing(feature, m_webFeatures, m_webFeaturesDisabled, enable);
}

bool KioskPermissions::permission(QWebEnginePage::Feature feature, bool defaultValue) const
{
    return isThingEnabled(feature, m_webFeatures, m_webFeaturesDisabled, defaultValue);
}

void KioskPermissions::setPermission(QWebEngineCertificateError::Error error, bool enable)
{
    enableThing(error, m_sslErrors, m_sslErrorsDisabled, enable);
}

bool KioskPermissions::permission(QWebEngineCertificateError::Error error, bool defaultValue) const
{
    return isThingEnabled(error, m_sslErrors, m_sslErrorsDisabled, defaultValue);
}

void KioskPermissions::setPermission(QWebEngineSettings::WebAttribute attribute, bool enable)
{
    enableThing(attribute, m_webAttributes, m_webAttributesDisabled, enable);
}

bool KioskPermissions::permission(QWebEngineSettings::WebAttribute attribute, bool defaultValue) const
{
    return isThingEnabled(attribute, m_webAttributes, m_webAttributesDisabled, defaultValue);
}

void KioskPermissions::setDownloadPermission(const QStringList &mimeTypes, const QHash<QString, QString> &mimeRunners)
{
    m_acceptableDownloadMimeTypes.clear();
    m_mimeRunners.clear();

    for (QString mimeType : mimeTypes) {
        QRegExp matcher(mimeType);
        matcher.setPatternSyntax(QRegExp::WildcardUnix);
        if (matcher.isValid()) {
            m_acceptableDownloadMimeTypes << matcher;
        } else {
            qWarning() << "Matcher error:" << matcher << mimeType;
        }
    }

    for (QString key : mimeRunners.keys()) {
        QRegExp matcher(key);
        matcher.setPatternSyntax(QRegExp::WildcardUnix);
        if (matcher.isValid()) {
            m_mimeRunners << QPair<QRegExp, QString>({matcher, mimeRunners[key]});
        } else {
            qWarning() << "Matcher error:" << matcher << key;
        }
    }
}

bool KioskPermissions::downloadPermission(const QString &mimeType, bool defaultValue) const
{
    for (QRegExp matcher : m_acceptableDownloadMimeTypes) {
        if (matcher.exactMatch(mimeType)) {
            return true;
        }
    }
    return defaultValue;
}

QString KioskPermissions::downloadRunner(const QString &mimeType) const
{
    for (QPair<QRegExp, QString> runner : m_mimeRunners) {
        if (runner.first.exactMatch(mimeType)) {
            return runner.second;
        }
    }
    return QString();
}

void KioskPermissions::configureSettings(QWebEngineSettings *settings) const
{
    for (uint attr = 0; attr <= QWebEngineSettings::AllowGeolocationOnInsecureOrigins; attr++) {
        settings->resetAttribute((QWebEngineSettings::WebAttribute)attr);
    }

    for (QWebEngineSettings::WebAttribute attr : m_webAttributes) {
        settings->setAttribute(attr, true);
    }

    for (QWebEngineSettings::WebAttribute attr : m_webAttributesDisabled) {
        settings->setAttribute(attr, false);
    }
}
