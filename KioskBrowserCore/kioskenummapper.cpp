/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "kioskenummapper.h"
#include <QMetaEnum>
#include <QMetaObject>

KioskEnumMapper::KioskEnumMapper(QObject *parent) : QObject(parent)
{
}

int KioskEnumMapper::stringToEnumValue(const QString &name, bool *ok) const
{
    QString enumName = name.section(QLatin1String("::"), 0, 0);
    QString enumValue = name.section(QLatin1String("::"), 1, 1);

    const QMetaObject *mo = metaObject();
    for (int idx = mo->enumeratorOffset(); idx < mo->enumeratorCount(); ++idx) {
        const QMetaEnum eo = mo->enumerator(idx);
        if (enumName.toLatin1() == eo.name()) {
            return eo.keyToValue(enumValue.toLatin1(), ok);
        }
    }

    if (ok) {
        *ok = false;
    }

    return -1;
}

QStringList KioskEnumMapper::enumNames() const
{
    QStringList result;
    const QMetaObject *mo = metaObject();
    for (int idx = mo->enumeratorOffset(); idx < mo->enumeratorCount(); ++idx) {
        const QMetaEnum eo = mo->enumerator(idx);
        result << QLatin1String(eo.name());
    }
    return result;
}
