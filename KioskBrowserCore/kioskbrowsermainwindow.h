/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BROWSERMAINWINDOW_H
#define BROWSERMAINWINDOW_H

#include "kioskbrowser_export.h"
#include <QMainWindow>

namespace Ui
{
class KioskBrowserMainWindow;
}

class KioskTabWidget;
class KioskWebView;
class KIOSK_CORE_EXPORT KioskBrowserMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit KioskBrowserMainWindow(QWidget *parent = 0);
    ~KioskBrowserMainWindow();

    KioskTabWidget *tabWidget() const;
    KioskWebView *currentTab() const;

protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void on_actionClose_triggered();
    void on_actionCut_triggered();
    void on_actionCopy_triggered();
    void on_actionPaste_triggered();

private:
    Ui::KioskBrowserMainWindow *ui;
};

#endif // BROWSERMAINWINDOW_H
