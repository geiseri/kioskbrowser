/*
 * Copyright (C) 2017  TCOS Maintainers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "kioskwebpage.h"
#include "kioskauthenticationdialog.h"
#include "kioskbrowser.h"
#include "kioskpermissions.h"

#include <QAction>
#include <QApplication>
#include <QAuthenticator>
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QTimer>

KioskWebPage::KioskWebPage(QObject *parent) : QWebEnginePage(KioskBrowser::instance()->profile(), parent)
{
    connect(this, &KioskWebPage::proxyAuthenticationRequired, this, &KioskWebPage::handleProxyAuthenticationRequired);
    connect(this, &KioskWebPage::authenticationRequired, this, &KioskWebPage::handleAuthenticationRequired);
    connect(this, &KioskWebPage::featurePermissionRequested, this, &KioskWebPage::handleFeaturePermissionRequested);
    connect(this, &KioskWebPage::fullScreenRequested, this, &KioskWebPage::handleFullScreenRequested);

    connect(this, &KioskWebPage::renderProcessTerminated,
        [this](QWebEnginePage::RenderProcessTerminationStatus termStatus, int statusCode) {
            Q_UNUSED(statusCode)
            switch (termStatus) {
            case QWebEnginePage::NormalTerminationStatus:
                qCritical() << "Render process normal exit";
                break;
            case QWebEnginePage::AbnormalTerminationStatus:
                qCritical() << "Render process abnormal exit";
                break;
            case QWebEnginePage::CrashedTerminationStatus:
                qCritical() << "Render process crashed";
                break;
            case QWebEnginePage::KilledTerminationStatus:
                qCritical() << "Render process killed";
                break;
            }
            QTimer::singleShot(0, [this] { triggerAction(QWebEnginePage::ReloadAndBypassCache); });
        });
}

KioskWebPage::~KioskWebPage()
{
}

void KioskWebPage::triggerAction(QWebEnginePage::WebAction action, bool checked)
{
    if (KioskBrowser::instance()->hasPermission(url(), action)) {
        QWebEnginePage::triggerAction(action, checked);
    }
}

void KioskWebPage::handleFeaturePermissionRequested(const QUrl &securityOrigin, QWebEnginePage::Feature feature)
{
    if (KioskBrowser::instance()->hasPermission(securityOrigin, feature)) {
        setFeaturePermission(securityOrigin, feature, QWebEnginePage::PermissionGrantedByUser);
    } else {
        setFeaturePermission(securityOrigin, feature, QWebEnginePage::PermissionDeniedByUser);
    }
}

void KioskWebPage::handleFullScreenRequested(QWebEngineFullScreenRequest fullScreenRequest)
{
    if (KioskBrowser::instance()->hasPermission(fullScreenRequest.origin(), KioskPermissions::AllowFullscreen)) {
        fullScreenRequest.accept();
    } else {
        fullScreenRequest.reject();
    }
}

QStringList KioskWebPage::chooseFiles(
    QWebEnginePage::FileSelectionMode mode, const QStringList &oldFiles, const QStringList &acceptedMimeTypes)
{
    qDebug() << Q_FUNC_INFO << oldFiles << acceptedMimeTypes;

    QStringList result;
    if (KioskBrowser::instance()->hasPermission(KioskPermissions::AllowUploads)) {
        if (QWebEnginePage::FileSelectOpen == mode) {
            result = QStringList() << QFileDialog::getOpenFileName(QApplication::activeWindow(),
                         tr("Select upload file"), KioskBrowser::instance()->downloadPath());
        } else if (QWebEnginePage::FileSelectOpenMultiple == mode) {
            result = QFileDialog::getOpenFileNames(
                QApplication::activeWindow(), tr("Select upload files"), KioskBrowser::instance()->downloadPath());
        }
    }

    return result;
}

void KioskWebPage::javaScriptAlert(const QUrl &securityOrigin, const QString &msg)
{
    if (KioskBrowser::instance()->hasPermission(securityOrigin, KioskPermissions::AllowPopups)) {
        QMessageBox::information(QApplication::activeWindow(), tr("Alert"), msg);
    }
}

bool KioskWebPage::javaScriptConfirm(const QUrl &securityOrigin, const QString &msg)
{
    if (KioskBrowser::instance()->hasPermission(securityOrigin, KioskPermissions::AllowPopups)) {
        int result = QMessageBox::question(
            QApplication::activeWindow(), tr("Confirm"), msg, QMessageBox::Yes, QMessageBox::No);
        return (result == QMessageBox::Yes);
    }
    return false;
}

bool KioskWebPage::javaScriptPrompt(
    const QUrl &securityOrigin, const QString &msg, const QString &defaultValue, QString *result)
{
    if (KioskBrowser::instance()->hasPermission(securityOrigin, KioskPermissions::AllowPopups)) {
        bool okay = false;
        QString value = QInputDialog::getText(
            QApplication::activeWindow(), tr("Prompt"), msg, QLineEdit::Normal, defaultValue, &okay);
        if (okay) {
            *result = value;
            return true;
        }
    }
    return false;
}

void KioskWebPage::javaScriptConsoleMessage(QWebEnginePage::JavaScriptConsoleMessageLevel level, const QString &message,
    int lineNumber, const QString &sourceID)
{
    // check debug level and output to qDebug
    switch (level) {
    case InfoMessageLevel:
        qDebug() << message << lineNumber << sourceID;
        break;
    case WarningMessageLevel:
        qWarning() << message << lineNumber << sourceID;
        break;
    case ErrorMessageLevel:
        qCritical() << message << lineNumber << sourceID;
        break;
    default:
        break;
    }
}

bool KioskWebPage::certificateError(const QWebEngineCertificateError &certificateError)
{
    // process certificate error acceptions, return true if its okay
    if (KioskBrowser::instance()->canIgnoreSslError(certificateError.url(), certificateError.error())) {
        return true;
    } else {
        return false;
    }
}

bool KioskWebPage::acceptNavigationRequest(const QUrl &url, QWebEnginePage::NavigationType type, bool isMainFrame)
{
    if (KioskBrowser::instance()->hasPermission(url, KioskPermissions::AllowVisitingUrl)) {
        if (isMainFrame) {
            KioskBrowser::instance()->configurePage(url, settings());
        }
        return QWebEnginePage::acceptNavigationRequest(url, type, isMainFrame);
    }

    return false;
}

void KioskWebPage::handleAuthenticationRequired(const QUrl &requestUrl, QAuthenticator *authenticator)
{
    KioskAuthenticationDialog dialog;
    dialog.setWindowTitle(tr("Login %1").arg(authenticator->realm()));
    if (dialog.exec() == QDialog::Accepted) {
        authenticator->setUser(dialog.username());
        authenticator->setPassword(dialog.password());
    } else {
        setUrl(QUrl());
    }
}

void KioskWebPage::handleProxyAuthenticationRequired(
    const QUrl &requestUrl, QAuthenticator *authenticator, const QString &proxyHost)
{
    // no clue what to do here
}

void KioskWebPage::handleUrlChanged(const QUrl &newUrl)
{
    qDebug() << Q_FUNC_INFO << newUrl;

    // KioskBrowser::instance()->configurePage(newUrl, settings());
}
